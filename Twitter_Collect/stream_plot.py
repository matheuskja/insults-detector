### Import all new modifications #########

import tweepy
import sys,os
import time
#print(os.getcwd())
# We import our access keys:
sys.path.append('../Twitter_Collection/Credentials')
sys.path.append(os.getcwd()+'/Credentials')
from credentials import * 
import json
from tweepy.streaming import StreamListener
import pandas as pd
import numpy as np
#Cassifier
sys.path.append(os.getcwd()+'/../Classifier')
sys.path.append(os.getcwd()+'/Classifier')
from naive_bayes import *
sys.path.append(os.getcwd()+'/../Extract')
from extract import *
from twitter_collect import *
import matplotlib.pyplot as plt

### Create the Plot with the x and y given by the user ##########
def plot_x_y(x,y,a,e):
    plt.subplot(131)
    plt.plot(x,y, color = 'blue')
    plt.xlabel("Replies")
    plt.ylabel("Insults")
    plt.title("Insults as a function of replies")
    plt.subplot(132)
    plt.plot(x,a, color = 'red')
    plt.xlabel("Replies")
    plt.ylabel("Rate")
    plt.title("Rate of insults replies for a user")
    plt.subplot(133)
    plt.plot(x,e, color = 'yellow')
    plt.xlabel("Replies")
    plt.ylabel("δ(insult-reply)")
    plt.title("Pics of insults")
    plt.pause(0.5)
    


### Create the class that we will use to stream ######
class StdOutListener_plot(StreamListener):
    ## initialize the class ###
    def __init__(self,name, train_csv):   
        ### get the name of the user we'll stream ####
        self.name = name;
        ### create the model to classify #######
        self.model = classifier_naive_bayes()
        ### get the texts and labels for training the model ######
        texts_train, labels_train = extract_filtered_data(train_csv)
        ### train the model ########
        self.model.train(texts_train,labels_train,alpha = 0.8)
        #### start the list of number of insults #####
        self.insults = []
        ### count the number of tweets ######
        self.count = 0
        #### get the number of insults ##########
        self.insults_count = 0
        #### start the list of rates of insults #####
        self.rates = []
        self.ecart =[]

    def on_data(self, data):
        ### transform the data received in format de dictionary #######
        tweet = json.loads(data)
        ### Verify if the tweet was a reponse to our name
        if(tweet['in_reply_to_screen_name'] == self.name ):
            ### create a connexion to tweet #####
            connexion = twitter_setup()
            ### get the tweet id of the former tweet ######
            tweet_id = tweet["in_reply_to_status_id"]
            #### if the teet_id exists #####
            if(tweet_id != None):
                ###Print the Tweet and its reply
                print("TWEET: " + connexion.get_status(int(tweet_id)).text)
                print("REPLY: " + tweet['text'])
                ###Verify if there is an insult
                ### If there is add in the counter_insult and add in the list insults #########
                ecart=0
                if(self.model.classify(tweet['text']) == 1):
                    print("There IS an insult \n\n\n")
                    self.insults_count += 1
                    ecart +=1
                else:
                    print("There is NOT an insult \n")
                    ecart=0

                self.insults.append(self.insults_count)
                self.ecart.append(ecart)
                self.count +=1
                ### Calculates the current rate ####
                self.rates.append((self.insults_count/self.count)*100)
                print("The insult rate form the beginning of the stream is : ")
                print(self.rates[-1])
                print('%\n\n\n')
                ## Plot the graph for [0,1,2,...,self.count] and self.insults
                plot_x_y([i for i in range(1,self.count+1)], self.insults,self.rates,self.ecart)
                
            else:
                print("ID doesn't exist")

        return True
## The function to plot the streaming
def plot_insults_by_streaming(name,train_csv = (os.getcwd()+'/../Data/train.csv')):
    ## open a connextion ###
    connexion = twitter_setup()
    ### Create a Object of the class StdOutListener_plot ##
    listener = StdOutListener_plot(name,train_csv)
    ### Create the stream ###
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    ### Keep calling on_data until it returns True ####
    stream.filter(track=[name])

