import tweepy
import sys,os
import time
# We import our access keys:
sys.path.append('../Twitter_Collection/Credentials')
sys.path.append(os.getcwd()+'/Credentials')
from credentials import * 
import json
from tweepy.streaming import StreamListener
import pandas as pd
import numpy as np
#Cassifier
sys.path.append(os.getcwd()+'/../Classifier')
sys.path.append(os.getcwd()+'/Classifier')
from naive_bayes import *
sys.path.append(os.getcwd()+'/../Extract')
from extract import *
from twitter_collect import *
import matplotlib.pyplot as plt

### Create the Plot with the x and y given by the user ##########
def plot_x_y(x,y,a,b):
    plt.subplot(131)
    plt.plot(x,y, color = 'blue')
    plt.xlabel("Replies")
    plt.ylabel("Insults")
    plt.title("Insults as a function of replies to a specific tweet in streaming")
    plt.subplot(132)
    plt.plot(x,a, color = 'red')
    plt.xlabel("Replies")
    plt.ylabel("Rate")
    plt.title("Rate of insults as a function of replies")
    plt.subplot(133)
    plt.plot(x,b, color = 'pink')
    plt.xlabel("Replies")
    plt.ylabel("δ(insult-reply)")
    plt.title("δ(insult-reply) in streaming")
    plt.pause(0.5)


### Create the class that we will use to stream ######
class StdOutListener_plot_specific(StreamListener):
    ## initialize the class ###
    def __init__(self,name, train_csv, id_reply):   
        ### get the name of the user we'll stream ####
        self.name = name
        self.id_reply = id_reply
        ### create the model to classify #######
        self.model = classifier_naive_bayes()
        ### get the texts and labels for training the model ######
        texts_train, labels_train = extract_filtered_data(train_csv)
        ### train the model ########
        self.model.train(texts_train,labels_train,alpha = 0.8)
        #### start the list of number of insults #####
        self.insults = []
        ### count the number of tweets ######
        self.count = 0
        #### get the number of insults ##########
        self.insults_count = 0
        #### start the list of rates of insults #####
        self.rates = []
        #### start the list of delta (Kronecker) ####
        self.delta = []

    def on_data(self, data):
        ### transform the data received in format de dictionary #######
        tweet = json.loads(data)
        ### Verify if the tweet was a reponse to our link ###
        if(tweet["in_reply_to_status_id"] == self.id_reply):
            ### create a connexion to tweet #####
            connexion = twitter_setup()
            ### get the tweet id of the former tweet ######
            tweet_id = tweet["in_reply_to_status_id"]
            #### if the teet_id exists #####
            if(tweet_id != None):
                ###Print the reply
                print("REPLY: " + tweet['text'])
                delta = 0
                ###Verify if there is an insult
                ### If there is add in the counter_insult and add in the list insults #########
                if(self.model.classify(tweet['text']) == 1):
                    print("There IS an insult \n\n\n")
                    self.insults_count += 1
                    delta+=1
                else:
                    print("There is NOT an insult \n")
                    delta = 0

                self.insults.append(self.insults_count)
                self.count +=1
                ### Calculates the current rate ####
                self.rates.append((self.insults_count/self.count)*100)
                self.delta.append(delta)
                print("The insult rate from the beginning of the comments of the tweet is : ")
                print(self.rates[-1])
                print('%\n\n\n')
                ## Plot the graph for [1,2,...,self.count] and self.insults and sel.rates and self.delta ##
                plot_x_y([i for i in range(1,self.count+1)], self.insults,self.rates, self.delta)
                
            else:
                print("ID doesn't exist")

        return True
## The function to plot the streaming
def plot_insults_by_streaming_specific(link,train_csv = (os.getcwd()+'/../Data/train.csv')):
    ## open a connextion ###
    connexion = twitter_setup()
    tweet_id = ''
    #The id of the targeted tweet is at the end of the link
    #so it gets this id
    for i in range(1,len(link)):
        if(link[-i]=='/'):
            break
        tweet_id = link[-i] + tweet_id
    #It gets the tweet by its link
    tweet_origin = connexion.get_status(tweet_id)
    name = tweet_origin.user.screen_name

    print("TWEET : " + tweet_origin.text)
    ### Create a Object of the class StdOutListener_plot ##
    listener = StdOutListener_plot_specific(name,train_csv,int(tweet_id))
    ### Create the stream ###
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    ### Keep calling on_data until it returns True ####
    stream.filter(track=[name])

