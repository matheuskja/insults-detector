import tweepy
import sys,os
import time
# We import our access keys:
sys.path.append('../Twitter_Collection/Credentials')
sys.path.append(os.getcwd()+'/Credentials')
from credentials import * 
import json
from tweepy.streaming import StreamListener
import pandas as pd
import numpy as np
#Cassifier
sys.path.append(os.getcwd()+'/../Classifier')
sys.path.append(os.getcwd()+'/Classifier')
#print(os.getcwd())
from naive_bayes import *
sys.path.append(os.getcwd()+'/../Extract')
from extract import *


def twitter_setup():
   # """
   # Utility function to setup the Twitter's API
   # with an access keys provided in a file credentials.py
   # :return: the authentified API
    #"""
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    api = tweepy.API(auth)
    return api

# Collects number of tweets (200 by default) of a user by it's user id
def collect_by_user(user_id, number_of_tweets = 200): 
    connexion = twitter_setup()
    statuses = connexion.user_timeline(id = user_id, count = number_of_tweets)   
    return statuses

#Collects retweets per page (= rpp) in the language you want (french by default) of a user by his name
def collect(name, langue = 'french',rpp_ = 100):
    connexion = twitter_setup()
    tweets = connexion.search(name,language=langue,rpp=rpp_)
    return tweets;

#Collects a tweet by it's id
def collect_by_id(tweet_id):
	connexion = twitter_setup()
	return connexion.get_status(tweet_id)

#Store the tweets you want in a Json or CSV file, showing the text, size, 
#date of creation, hashtags,  id of the tweet, references, and the number 
#of retweets of your tweets
def store_tweets(tweets,filename,Json = True, CSV = False):
    data = []
    data_csv = []
    for tweet in tweets:
        ## Getting the references of each tweets
    	ref = [tweet.entities['user_mentions'][i]['screen_name'] for i in range(len(tweet.entities['user_mentions']))]
    	if(tweet.in_reply_to_screen_name == None):
    		pass
    	else:
            ##if the tweet is responding to a tweet, it gets the name of the tweet in reply to
    		ref = ref + [tweet.in_reply_to_screen_name];
        # it fills the list data with Json format
    	data.append({'text':tweet.text,'size': len(tweet.text) , 'date':tweet.created_at,'hashtag': [hashtag['text'] for hashtag in tweet.entities['hashtags']],'id': tweet.id,'Reference':ref,'Retweet_count':tweet.retweet_count})
    	data_csv.append([tweet.text,len(tweet.text),tweet.created_at,[hashtag['text'] for hashtag in tweet.entities['hashtags']],tweet.id, ref,tweet.retweet_count])
    #If the Json format has been chosen
    if(Json):
    	with open(filename, 'w') as f:
    		str_json = json.dumps(data, indent=4, sort_keys=True, default=str)
    		f.write(str_json)

    columns = ['text','size','date','hashtag','id','Reference','Retweet_count' ]
    #If the CSV format has been chosen
    if(CSV):
    	pd.DataFrame(np.array(data_csv), columns=columns).to_csv(filename+'.csv')


    return pd.DataFrame(np.array(data_csv), columns=columns)


#Transform your tweets into a dataframe with the columns text, size, date, hashtag, id, Retweet_count, Reference
def transform_to_dataframe(tweets):
    columns = ['text','size', 'date', 'hashtag', 'id','Retweet_count','Reference' ]
    data = []
    
    #Getting the references
    for tweet in tweets:
    	ref = [tweet.entities['user_mentions'][i]['screen_name'] for i in range(len(tweet.entities['user_mentions']))]
    	if(tweet.in_reply_to_screen_name == None):
    		pass
    	else:
    		ref = ref + [tweet.in_reply_to_screen_name];

        # It fills the list data
    	data.append([tweet.text,len(tweet.text), tweet.created_at, [hashtag['text'] for hashtag in tweet.entities['hashtags']], tweet.id,tweet.retweet_count,ref])    	
    return pd.DataFrame(np.array(data), columns=columns)


def load_to_data_frame(f_name, store = False):
    ##openning the file##
    with open(f_name,'r') as f:
    	data = json.load(f)
    ##making columns of CSV###
    columns = list(data[0].keys())
    print(columns)
    data_frame = []
    ##getting the values from the dictionary###
    for d in data:
    	data_frame.append([d[key] for key in columns])
    ##Making the DataFrame####
    data_frame = pd.DataFrame(np.array(data_frame),columns=columns)
    
    ##if the option of storing is selected, Store##
    if(store):
        data_frame.to_csv(f_name + '.csv')

    return data_frame

#Gets the replies of a tweet thanks to its id or its link
def get_replies_to_tweet(tweet_id, link_tweet = False, count = 200):
    connexion = twitter_setup()
    if(link_tweet):
        tweet = collect_by_link(tweet_id)
    else:
        tweet = collect_by_id(tweet_id)
    #It gets the 200 lasts tweets in reply to the user of the tweet 'tweet'
    answer = connexion.search('to:'+tweet.user.screen_name ,language='french',count=2000)
    answer_twitter = []
    for ans in answer:
        #If the tweet 'ans' replies to the tweet 'tweet'
    	if(ans.in_reply_to_status_id == tweet.id):
    		#It stores it in the final list
            answer_twitter.append(ans)
    
    return answer_twitter

#It collects retweets from the last tweet of the user 'num_candidate'
def collect_retweets(num_candidate):
    connexion = twitter_setup()
    statuses = connexion.user_timeline(id = num_candidate, count = 1)
    res=["ID du tweet : " + "statuses.id" + " et les personnes qui ont retweeté ce tweet sont : "]
    #It goes throught all the retweets
    for r in statuses[0].retweets():
        #It fills the list with the screen name of the retweets
        res.append(r.author.screen_name)
    print(res)


#Streaming
#It streams all the tweets in reply to the user 'name'
class StdOutListener_aux(StreamListener):
    def __init__(self,name):   
        self.name = name;

    def on_data(self, data):
        #Converts the data into a json object in order to get the information easier
        tweet = json.loads(data)
        #If the tweet replies to the user 'name' ...
        if(tweet['in_reply_to_screen_name'] == self.name ):
            connexion = twitter_setup()
            tweet_id = tweet["in_reply_to_status_id"]
            #... and if the id of the tweet in response to exists
            if(tweet_id != None):
                #It displays the id, the tweet and the corresponding reply
                print(tweet_id)
                print("TWEET: " + connexion.get_status(int(tweet_id)).text)
                print("REPLY: " + tweet['text'] + "\n\n\n")
            else:
                print("ID doesn't exist")

        return True
    #If the number of streamed tweets is too large, it stops the program
    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True

def collect_by_streaming(name):
    connexion = twitter_setup()
    listener = StdOutListener_aux(name)
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    #It streams, tracking the 'name' chosen
    stream.filter(track=[name])


#Insults streaming
class StdOutListener_insult(StreamListener):
    def __init__(self,name, train_csv):   
        self.name = name;
        self.model = classifier_naive_bayes()
        #using the train file for the naive bayse
        texts_train, labels_train = extract_filtered_data(train_csv)
        #Trains the model
        self.model.train(texts_train,labels_train,alpha = 0.8)

    def on_data(self, data):
        tweet = json.loads(data)
        
        if(tweet['in_reply_to_screen_name'] == self.name ):
            connexion = twitter_setup()
            tweet_id = tweet["in_reply_to_status_id"]
            if(tweet_id != None):
                #print(tweet_id)
                print("TWEET: " + connexion.get_status(int(tweet_id)).text)
                print("REPLY: " + tweet['text'])
                #Shows if the reply is an insult or not
                if(self.model.classify(tweet['text']) == 1):
                    print("There IS an insult \n\n\n")
                else:
                    print("There is NOT an insult \n\n\n")

                time.sleep(2);
            else:
                print("ID doesn't exist")

        return True

#Uses the file train.csv by default
def collect_insults_by_streaming(name,train_csv = (os.getcwd()+'/../Data/train.csv')):
    connexion = twitter_setup()
    listener = StdOutListener_insult(name,train_csv)
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=[name])



#Prints the text of a tweet
def print_tweet(tweet, param = None):
	if(param == None):
		print(tweet.text)

#Prints the text of tweets
def print_tweets(tweet, param = None):
	for i in tweet:
		print_tweet(i,param)

#Gets the tweet thanks the wek link
def collect_by_link(link):
	connexion = twitter_setup()
	tweet_id = ''
    #The id of the targeted tweet is at the end of the link
    #so it gets this id
	for i in range(1,len(link)):
		if(link[-i]=='/'):
			break
		tweet_id = link[-i] + tweet_id
    #Calls the function 'collect_by_id' using the id of the tweet 
	return collect_by_id(int(tweet_id))