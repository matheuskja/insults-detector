import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import roc_auc_score

class classifier_naive_bayes:
	def __init__(self, file_badwords = ''):

		self.model = None;
		self.vect = None;


	def classify(self,comment):
		if(self.model != None and self.vect != None):
			return self.model.predict(self.vect.transform([comment]))
		else:
			print("It's necessary that you train the model first")


	def score(self, comments,labels):
		count_right = 0
		count = 0
		for comment,label in zip(comments,labels):
			if(label == self.classify(comment)):
				count_right += 1
			count = count + 1
		return count_right/count


	def train(self,text,labels,alpha = 1.23):
		#### Vectorizer the text for using the model in Sci-Kit#####
		self.vect = CountVectorizer().fit(text)
		text_vectorized = self.vect.transform(text)
		### Calling the Sci-Kit Model####
		self.model = MultinomialNB(alpha = alpha)
		self.model.fit(text_vectorized, labels)
