from classifier import *
from naive_bayes import *
import sys,os
sys.path.append('../Extraction')
from extract import *


def feature_selection(filename,n=20):
	score_best = 0
	n_best = 0
	model = classifier()
	texts, labels = extract_filtered_data(filename)
	for i in range(n):
		score = model.score(texts,labels,n=i)
		if(score > score_best):
			n_best = i
			score_best = score
	return n_best,score_best

def feature_selection_naive_bayes( train_file = '../Data/train.csv', test_file = '../Data/test_with_solutions.csv',n=10):
	score_best = 0
	n_best = 0
	model = classifier_naive_bayes()
	texts_train, labels_train = extract_filtered_data(train_file)
	texts, labels = extract_filtered_data(test_file)
	for i in range(n):
		model.train(texts_train,labels_train,alpha = 0.7+i*0.01)
		score = model.score(texts,labels)
		if(score > score_best):
			n_best = 0.7+i*0.01
			score_best = score
	return n_best,score_best

