from sklearn.model_selection import cross_val_score
from naive_bayes import *
from SVM import *

import os,sys
os.chdir('../Extract')
sys.path.append(os.getcwd())
from extract import *

from sklearn.feature_extraction.text import CountVectorizer

os.chdir('../Classifier')

import numpy as np

def validadate_naive_bayes():
	model = classifier_naive_bayes()
	train, label_train = extract_filtered_data('../Data/train.csv')
	test, label_test = extract_filtered_data('../Data/test_with_solutions.csv')

	text = train + test
	label = label_train + label_test

	vec = CountVectorizer().fit(text)

	text_vec = vec.transform(text).toarray()
	scores = cross_val_score(model.model, text_vec, label, cv=5)
	return scores

#print(np.mean(validadate_naive_bayes()))


def validadate_svm():
	model = classifier_svm()
	train, label_train = extract_filtered_data('../Data/train.csv')
	test, label_test = extract_filtered_data('../Data/test_with_solutions.csv')

	text = train + test
	label = label_train + label_test

	vec = CountVectorizer().fit(text)

	text_vec = vec.transform(text).toarray()
	scores = cross_val_score(model.model, text_vec, label, cv=5)
	return scores

print(np.mean(validadate_svm()))