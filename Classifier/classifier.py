import pandas as pd

class classifier:
	def __init__(self, file_badwords = ''):
		try:
			with open(file_badwords,'r') as f:
				self.badwords = [row[0:-1] for row in f]
		except:
			print("File doesn't exist, using default badwords")
			with open('../Data/badwords.txt','r') as f:
				self.badwords = [row[0:-1] for row in f]


	def classify(self,comment,n = 3):
		count = 0;
		for word in self.badwords:
			if(word in comment):
				count = count + 1

		if(count > n):
			return 1
		else:
			return 0;

	def score(self, comments,labels,n = 3):
		count_right = 0
		count = 0
		for comment,label in zip(comments,labels):
			if(label == self.classify(comment,n)):
				count_right += 1
			count = count + 1
		return count_right/count
