import sys
sys.path.append('../Extract')
sys.path.append('../Classifier')
sys.path.append('../Twitter_Collect')
sys.path.append('../Twitter_Collect/Credentials')
from extract import *
from classifier import *
from naive_bayes import *
from SVM import *
from features_selection import *
from twitter_collect import *


############### TESTING Extract####################
def test_extract_sentence(filename):
	text, label = extract_sentence(filename)
	if(len(text)>0):
		print("\nOk\n")
	else:
		print("\nNot OK\n")
	return text, label

def test_filter_data(texts,labels):
	text, label = filter_data(texts,labels)
	if(len(text)>0):
		print("\nOk\n")
	else:
		print("\nNot OK\n")
	return text, label

def test_extract_filtered_data(file_name):
	text, label = extract_filtered_data(file_name)
	if(len(text)>0):
		print("\nOk\n")
	else:
		print("\nNot OK\n")
	return text, label

#texts,labels = test_extract_sentence('../Data/train.csv')
#print(texts)
#test_filter_data(texts,labels)
#print(test_extract_filtered_data('../Data/train.csv')[0])

############ TESTING Classifier #######################
def test_classifier_score(filename):
	model = classifier()
	texts, labels = extract_filtered_data(filename)
	print(model.score(texts, labels))

#test_classifier_score('../Data/test_with_solutions.csv')


########### TESTING FILTER SELECTION ################
def test_feature_selection(filename, n= 20):
	print(feature_selection(filename, n))

#test_feature_selection('../Data/test_with_solutions.csv')


######## TESTING Naive_bayes ##########################
def test_naive_bayes(train_file = '../Data/train.csv', test_file = '../Data/test_with_solutions.csv'):
	model = classifier_naive_bayes()
	texts_train, labels_train = extract_filtered_data(train_file)
	model.train(texts_train,labels_train,alpha = 1.23)
	texts_test, labels_test = extract_filtered_data(test_file)
	print(model.score(texts_test, labels_test))
#test_naive_bayes()

########### TESTING FILTER SELECTION ################
def test_feature_selection_naive_bayes( n= 1):
	print(feature_selection_naive_bayes(n = n))

#test_feature_selection_naive_bayes(40)

############ TESTING SVM ####################
def test_svm(train_file = '../Data/train.csv', test_file = '../Data/test_with_solutions.csv'):
	model = classifier_svm()
	texts_train, labels_train = extract_filtered_data(train_file)
	model.train(texts_train,labels_train, gamma = 0.04)
	texts_test, labels_test = extract_filtered_data(test_file)
	print(model.score(texts_test, labels_test))
test_svm()


############## TESTING Twitter_Collect ##################
def test_collect_by_streaming(name):
	collect_by_streaming(name)
#test_collect_by_streaming('jairbolsonaro')
def test_collect_by_id(id):
	print_tweet(collect_by_id(id))

#test_collect_by_id(1195357876770000896)


def test_get_replies_to_tweet(tweet_id):
	print_tweets(get_replies_to_tweet(tweet_id))
#test_get_replies_to_tweet(1195357876770000896)

def test_collect_by_link(id):
	print_tweet(collect_by_link(id))
#test_collect_by_link('https://twitter.com/NazimDadi1/status/1194643870744748032')

def test_store_json(id):
	tweet = collect_by_id(id)
	store_tweets([tweet], '../teste.json')

#test_store_json(1195357876770000896)

def test_collect_insults_by_streaming(name, file_train = '../Data/train.csv'):
	collect_insults_by_streaming(name,file_train)

#test_collect_insults_by_streaming('realDonaldTrump')

def test_get_replies_to_tweet(tweet_id, link = False):
	print_tweets(get_replies_to_tweet(tweet_id, link_tweet = link))
#test_get_replies_to_tweet('https://twitter.com/NazimDadi1/status/1194643870744748032', link = True)

