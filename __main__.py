###### In this File we are going to make a interactive program that takes the main functions that we made ########## 
###### First We have to import all the modules with the functions made ################
###### we have utilized os.chdir to make the path correct to satisfy the imports condition ############
import os,sys
os.chdir('Twitter_Collect')
sys.path.append(os.getcwd())
from Twitter_Collect.twitter_collect import *
from Twitter_Collect.stream_plot import *
from Twitter_Collect.stream_plot_link import *
os.chdir('../Classifier')
sys.path.append('Classifier/')
from Classifier.classifier import *
from naive_bayes import *
os.chdir('../Extract')
from Extract.extract import *
os.chdir('../')
import matplotlib.pyplot as plt


#### Here we Start the Program ###########

def main(train_file = 'Data/train.csv'):
	## we utilize anser to continue asking the user what he wants to do ######
	answer = ''
	### We create our model of Classifier #### We used Naive Bayes because it was the most effective one ######
	model = classifier_naive_bayes()
	### we get the texts and labels from train to train the model ######
	texts_train, labels_train = extract_filtered_data(train_file)
	## we train the model ##########
	model.train(texts_train,labels_train,alpha = 1.2)

	### keep a loop asking the user to do new functionalities #######
	while(answer != "q"):
		##### answer of the user for the options #########
		answer = input("What do you want to do?Options:\n a - Analyse a tweet its link.\n b- Analyse a comment by an Archive txt.\n c- Analyse a tweet by an archive JSON\n d - Analyse the following sentence :\n e - Analyse a tweet by its ID.\n f - Analyse if there is an insult in (streaming).\n g - Show the evolution of insults in the last replies of a tweet (not in streaming).\n h - Analyse if there is an insult in replies to a username (in streaming).\n i - Analyse if there is an insult in reply to a specific tweet thanks to its link (in streaming).\n q - Quit\nR:")
		#### verifying which option he chose. ########
		if(answer == 'a'):
			### get the link of the tweet ############
			lien_tweet = input("Please put link of tweet: ")
			#### get that tweet #######
			tweet = collect_by_link(lien_tweet)
			##### filter le tweet text ########
			text = filter_data([tweet.text],[0])[0]
			print("\nTEXT = ",text[0])
			#### classify the text #######
			insult = model.classify(text[0])

			##### verify if there is un Insult #########
			if(insult == 1):
				print("\n#######################\nthis tweet is an insult\n#####################################\n")
			else:
				print("\n#######################\nthis tweet is NOT an insult\n#####################################\n")
	
		elif(answer == 'b'):
			#### Put the file name ########
			file_txt = input("Please put name of the file: ")
			#### Open the file and read it #######
			with open(file_txt,'r') as f:
				tweet = f.read()

			### filter the text that is in the text ########
			tweet = filter_data([tweet],[0])[0]

			print("\nTEXT = ",tweet[0])
			### Classify the tweet #########
			insult = model.classify(tweet[0])
			### verify if there is an insult ###########
			if(insult == 1):
				print("\n#######################\nThis comment is an insult\n#####################################\n")
			else:
				print("\n#######################\nThis comment is NOT an insult\n#####################################\n")
	
		elif(answer == 'c'):
			##### put the name of the file #######
			file_txt = input("Please put name of the file: ")
			#### Open the file and read it #######
			with open(file_txt, 'r') as f:
				tweet = json.load(f)

			### filter the text that is in the text ########
			tweet = filter_data([tweet[0]['text']],[0])[0]
			print("\nTEXT = ",tweet[0])
			#### classify the text #######
			insult = model.classify(tweet[0])
			#### verify if there is an insult ################
			if(insult == 1):
				print("\n#######################\nThis comment is an insult\n#####################################\n")
			else:
				print("\n#######################\nThis comment is NOT an insult\n#####################################\n")
		elif(answer == 'd'):
			### get the text from the user #######
			commentaire = input("Please put the text: ")
			##### Filter the text ##########
			commentaire = filter_data([commentaire],[0])[0]
			print("\nTEXT = ",commentaire[0])
			##### Classify the text ############
			insult = model.classify(commentaire[0])
			##### Verify if there is an insult ###############
			if(insult == 1):
				print("\n#######################\nThis text contains insults\n#####################################\n")
			else:
				print("\n#######################\nThis text does NOT contains insults\n#####################################\n")
		elif(answer == 'e'):
			### Get the id of the tweet with the user #########
			id_tweet = input("Please put id of tweet: ")
			### Get the tweet ########
			tweet = collect_by_id(int(id_tweet))
			#### treat the text of the tweet #########
			text = filter_data([tweet.text],[0])[0]
			print("\nTEXT = ",text[0])
			#### Classify the text ###########
			insult = model.classify(text[0])
			#### Verify if there is an Insult #############
			if(insult == 1):
				print("\n#######################\nthis tweet is an insult\n#####################################\n")
			else:
				print("\n#######################\nthis tweet is not an insult\n#####################################\n")

		elif(answer == 'f'):
			#### Get the name of the user #############
			name = input("Please put username that you want to follow: ")
			### get the insullts replies in stream ###########
			collect_insults_by_streaming(name)

		elif(answer == 'g'):
			### Ask if the twitter will be given by a Link or ID ###########
			answer = input("Do you want to analyse a tweet from\n \'A' a link or\n \'B' a id\nR:")
			###### get the answer #######
			if( answer.lower() == 'a'):
				#### get the link of the tweet############
				lien_tweet = input("Please put link of tweet: ")
				## get the tweet of this link #########
				tweets = get_replies_to_tweet(lien_tweet, link_tweet = True)
			else:
				#### get the id od the tweet ###############
				id_tweet = input("Please put id of tweet: ")
				#### get the tweet from its id #######
				tweets = get_replies_to_tweet(id_tweet, link_tweet = False)

			### count how many tweets there is #########
			count = [i for i in range(len(tweets))]
			### Start counting the number of insults ########
			count_insults = 0
			##### create a list of number of insults by time ############## 
			insults = []
			### for each twitter verify if there is a Insult
			### If there is increment the count_insults
			### after add the number of insults in the list 'insults ##################'
			for tweet in tweets:
				text = filter_data([tweet.text],[0])[0][0]
				print(text)
				insult = model.classify(text)
				if(insult == 1):
					count_insults += 1
				insults.append(count_insults)

			### Print the lists insults and count ##########
			print(insults)
			print(count)

			### plot the numer of insult versus the number of tweets #######
			plt.plot(count,insults)
			## add the name of X, Y axis and the Title of the graph ###
			plt.xlabel("Replies")
			plt.ylabel("Insults")
			plt.title("Graphe représentatif du nombre d'insultes dans les réponses")
			plt.show()
			print('\n')
		elif(answer == 'h'):
			### get the username from the user ############
			name = input("Please put username that you want to follow: ")
			#### plot the insults by stream ###########
			plot_insults_by_streaming(name)

		elif(answer == 'i'):
    			### get the link of the tweet ############
			link = input("Please put the link of the tweet you want to follow: ")
			#### plot the insults by stream ###########
			plot_insults_by_streaming_specific(link)



main()
