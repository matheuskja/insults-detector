import pandas as pd
import numpy as np
import string

def extract_sentence(file_name):
	data = pd.read_csv(file_name)
	texts = np.array(data['Comment'])
	labels = None
	if('Insult' in data):
		labels = np.array(data['Insult'])

	return texts, labels

def filter_data(texts,labels):
	texts_filter = []
	labels_filter = []
	for text,label in zip(texts,labels):

		signal = 0
		text_aux = ''
		for letter in text.lower():
			if(letter not in (string.ascii_lowercase + '0123456789.-?\'' + ' ') or signal == 1):
				if(letter == '\\' or signal == 1):
					if(letter == '\\'):
						signal = 1;
						continue;
					else:
						if(letter != '\''):
							text_aux = text_aux + ' '
							signal = 0
							continue;

				continue;

			text_aux = text_aux + letter

		text_aux = text_aux.lower()
		label_aux = label

		texts_filter.append(text_aux)
		labels_filter.append(label_aux)

	return (texts_filter),(labels_filter)

def extract_filtered_data(file_name):
	texts, labels = extract_sentence(file_name)
	return filter_data(texts,labels)

