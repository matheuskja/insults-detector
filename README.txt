Groupe 5
Ce projet consiste à détecter des insultes sur Twitter. 

=======REQUIREMENTS=======
AVANT DE COMMENCER verifier que vous avez les REQUIREMENTS:
Pour créer un compte développeur Twitter, faire une demande sur le site Twitter (attention, la validation de votre demande peut prendre un certain temps). Le compte créé, créez une app. Puis notez dans un fichier credentials.py vos clés d'accès et de consumer de cette manière:
# Twitter App access keys for @user
Créer après un dossier credentials où il faut créer un fichier python credentials.py qui aura la format suivante(à remplir) :
# Consume:
CONSUMER_KEY    = ''
CONSUMER_SECRET = ''

# Access:
ACCESS_TOKEN  = ''
ACCESS_SECRET = ''

Puis suivre la liste d'instruction suivante : 
-Installer la derniere version Python via https://www.python.org/downloads/ 
-Installer tweepy via pip install tweepy sur votre terminal.
-Installer matplotlib via pip install matplotlib sur votre terminal.
-Installer pandas via pip install pandas sur votre terminal.
-Installer sklearn via pip install sklearn sur votre terminal.
-Installer numpy via pip intall numpy sur votre terminal
-Si vous n'avez pas la bibliothèque json, l'installer via pip install json sur votre terminal 
--------- si jamais 'pip' ne marche pas penser à utiliser 'pip3'---------


=======EXECUTION=======
------>Exécuter __main__.py dans votre terminal et choisir l'approche que vous voulez suivre pour analyser les tweets.
a- Copiez le lien du tweet que vous voulez analyser
b- Mettez le fichier .txt que vous voulez analyser
c- Mettez le fichier Json que vous voulez analyser
d- Ecrivez la phrase que vous voulez analyser
e- Copiez l'ID du tweet que vous voulez analyser
f- Entrez le nom d'utilisateur dont vous voulez analyser les réponses à ses tweets (en streaming)
g- Entrez le lien ou l'ID du tweet dont vous voulez analyser les dernières réponses (pas en streaming)
h- Entrez le nom d'utilisateur dont vous voulez analyser les réponses à ses tweets et afficher l'évolution graphique des insultes (en streaming)
i- Entrez lien du tweet dont vous voulez analyser les réponses et afficher l'évolution graphique des insultes(en streaming)


=======PRECISION=======
Nous avons utilisé au final l'algorithme naive_bayes et nous avons validé le modèle avec CROSSVALiDATION avec 5 fold.
