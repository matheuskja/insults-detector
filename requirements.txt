Etapes à suivre pour profiter au mieux de notre projet: il vous faudra installer plusieurs modules et créer un compte dévoloppeur Twitter.
Pour créer un compte développeur Twitter, faire une demande sur le site Twitter (attention, la validation de votre demande peut prendre un certain temps). Le compte créé, créez une app. Puis notez dans un fichier credentials.py vos clés d accès et de consumer de cette manière:
# Twitter App access keys for @user
Créer après un dossier credentials où il faut créer un fichier python credentials.py qui aura la format suivante(à remplir) :
# Consume:
CONSUMER_KEY    = ''
CONSUMER_SECRET = ''

# Access:
ACCESS_TOKEN  = ''
ACCESS_SECRET = ''

Concernant les modules à installer, taper dans votre tableau de commande:
- pip install tweepy
- pip install panda
- pip install numpy
- pip install matplotlib
- pip install sklearn
- Si vous n'avez pas la bibliothèque json, l'installer via pip install json sur votre terminal
--------- si jamais 'pip' ne marche pas penser à utiliser 'pip3'---------

Assurez vous bien d avoir ouvert tous les fichiers du projet car ils sont nécessaires à la bonne exécution du code. 
